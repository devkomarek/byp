FROM continuumio/miniconda as anaconda
WORKDIR /app
COPY environment.yml environment.yml
RUN /opt/conda/bin/conda env create -f environment.yml

FROM ubuntu
WORKDIR /app
ENV PYTHONUNBUFFERED 1
COPY --from=anaconda /opt/conda /opt/conda
COPY . .
ENTRYPOINT /opt/conda/envs/byp/bin/python ./manage.py runserver 0.0.0.0:8000 --settings=byp.settings.production