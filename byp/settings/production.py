from .base import *

DEBUG = False

ALLOWED_HOSTS = ['*']  # TODO replace

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'byp',
        'USER': 'komarek',
        'PASSWORD': 'komarek',
        'HOST': 'psql-container',
        'PORT': '5432',
    }
}

CELERY_BROKER_URL = 'redis://redis-container:6379'
