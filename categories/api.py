from rest_framework import viewsets

from . import serializers
from .models import Category


class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.CategorySerializer
    queryset = Category.objects.all()
