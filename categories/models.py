from django.core.validators import MaxValueValidator
from django.db import models


# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.CharField(max_length=500, null=True)
    priority = models.IntegerField(validators=[MaxValueValidator(10)])

    def __str__(self):
        return self.name
