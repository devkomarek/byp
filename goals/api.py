from rest_framework import viewsets

from . import serializers
from .models import Goal


class GoalViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.GoalSerializer
    queryset = Goal.objects.all()
