from django.db import models

# Create your models here.


class Goal(models.Model):
    lvl = models.IntegerField()
    exp = models.BigIntegerField()
    description = models.CharField(max_length=500)
    category_link = models.ManyToManyField('categories.Category')
    previous_goal = models.ForeignKey('self', models.CASCADE, null=True, related_name='previous')
    next_goal = models.ForeignKey('self', models.CASCADE, null=True, related_name='next')
    achievement = models.BooleanField(default=False)
