find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
rm -rf db.sqlite3
python manage.py makemigrations --settings="byp.settings.development"
python manage.py migrate --settings="byp.settings.development"
python manage.py loaddata fixtures/* --settings="byp.settings.development"
