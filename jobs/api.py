from datetime import datetime

from rest_framework import viewsets
from rest_framework.response import Response

from . import serializers
from .models import Job


class JobViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.JobSerializer
    queryset = Job.objects.all()

    def list(self, request, *args, **kwargs):
        if request.GET.get('when') == 'today':
            job = Job.objects.filter(date__exact=datetime.now()).get()
            return Response(serializers.JobSerializer(job).data)
        return super(JobViewSet, self).list(request, args, kwargs)
