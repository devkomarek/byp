from rest_framework import viewsets

from . import serializers
from .models import DailyGoal


class DailyGoalViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.DailyGoalSerializer
    queryset = DailyGoal.objects.all()
