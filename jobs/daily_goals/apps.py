from django.apps import AppConfig


class DailyGoalsConfig(AppConfig):
    name = 'jobs.daily_goals'
