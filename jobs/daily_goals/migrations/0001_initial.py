# Generated by Django 2.2.1 on 2019-06-29 10:44

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DailyGoal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=500)),
                ('exp', models.BigIntegerField(default=0)),
                ('achievement', models.BooleanField(default=False)),
            ],
        ),
    ]
