from django.db import models


class DailyGoal(models.Model):
    description = models.CharField(max_length=500)
    exp = models.BigIntegerField(default=0)
    achievement = models.BooleanField(default=False)

    def __str__(self):
        return self.description
