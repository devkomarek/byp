from rest_framework import routers

from .api import DailyGoalViewSet

router = routers.DefaultRouter()
router.register('daily-goals', DailyGoalViewSet, 'daily-goals')

urlpatterns = router.urls
