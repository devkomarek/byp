from django.db import models

# Create your models here.
from .tasks.models import Task


class Job(models.Model):
    date = models.DateField(null=False)
    tasks = models.ManyToManyField(Task)
    daily_goal = models.OneToOneField('daily_goals.DailyGoal', on_delete=models.CASCADE)

    def __str__(self):
        return 'Job: ' + str(self.date)
