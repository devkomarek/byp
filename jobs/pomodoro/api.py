from rest_framework import viewsets
from rest_framework.response import Response

from jobs.tasks.models import Task
from jobs.tasks.serializers import TaskSerializer
from . import serializers
from .tasks import start_timer_for_task


class PomodoroViewSet(viewsets.ViewSet):
    serializer_class = serializers.PomodoroSerializer
    queryset = "__all__"

    def create(self, request, job_pk=None, task_pk=None):
        task = Task.objects.get(pk=task_pk)
        if not task.achievement:
            start_timer_for_task.delay(task_pk)
        return Response(TaskSerializer(task).data)
