import time
from datetime import timedelta

from celery import shared_task

from jobs.tasks.models import Task
from django.conf import settings


@shared_task
def start_timer_for_task(task_pk):
    task = Task.objects.get(pk=task_pk)
    if task.achievement:
        return
    for t in range(settings.POMODORO_TIME):
        time.sleep(settings.POMODORO_REFRESH)
        task.time_work += timedelta(minutes=settings.POMODORO_REFRESH)
    if task.time_work > task.time_planed:
        task.achievement = True
    task.save()
