from datetime import timedelta

from django.conf import settings
from django.test import TestCase

from categories.models import Category
from jobs.daily_goals.models import DailyGoal
from jobs.tasks.models import Task
from .tasks import start_timer_for_task


class PomodoroTest(TestCase):
    def setUp(self):
        settings.POMODORO_TIME = 1
        settings.POMODORO_REFRESH = 0.1
        DailyGoal.objects.create(pk=1, description='desc_test')
        Category.objects.create(pk=1, name='category_test', description='desc_test', priority=5)

    def test_achievement_task_when_time_work_is_greater_than_time_planed(self):
        Task.objects.create(pk=1, category_id=1, time_planed=timedelta(seconds=0.1))
        start_timer_for_task(1)
        self.assertTrue(Task.objects.get(pk=1).achievement)

    def test_skip_when_task_is_achievement_and_run_pomodoro_again(self):
        Task.objects.create(pk=2, category_id=1, time_planed=timedelta(seconds=60), time_work=timedelta(seconds=60),
                            achievement=True)
        start_timer_for_task(2)
        self.assertTrue(Task.objects.get(pk=2).achievement)
