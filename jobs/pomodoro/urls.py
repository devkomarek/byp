from rest_framework import routers

from .api import PomodoroViewSet

router = routers.DefaultRouter()
router.register('pomodoro', PomodoroViewSet, 'pomodoro')

urlpatterns = router.urls
