from rest_framework import serializers

from .daily_goals.serializers import DailyGoalSerializer
from .models import Job
from .tasks.serializers import TaskSerializer


class JobSerializer(serializers.ModelSerializer):
    tasks = TaskSerializer(many=True, read_only=True)
    daily_goal = DailyGoalSerializer(many=False, read_only=True)

    class Meta:
        model = Job
        fields = ['date', 'tasks', 'daily_goal']
