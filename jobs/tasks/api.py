from rest_framework import viewsets

from . import serializers
from .models import Task


class TaskViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.TaskSerializer
    queryset = Task.objects.all()
