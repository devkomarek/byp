from django.db import models
from datetime import timedelta

# Create your models here.


class Task(models.Model):
    category = models.ForeignKey('categories.Category', null=False, on_delete=models.CASCADE)
    time_planed = models.DurationField()
    time_work = models.DurationField(blank=True, default=timedelta(minutes=0))
    achievement = models.BooleanField(default=False)

    def __str__(self):
        return 'Task: ' + self.category.__str__()

