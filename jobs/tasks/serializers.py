from rest_framework import serializers

from categories.serializers import CategorySerializer
from .models import Task


class TaskSerializer(serializers.ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = Task
        fields = ['category', 'time_planed', 'time_work', 'achievement']
