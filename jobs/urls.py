from django.urls import path, include
from rest_framework import routers

from .api import JobViewSet
from .pomodoro import urls

router = routers.DefaultRouter()
router.register('', JobViewSet, 'jobs')

urlpatterns = [
    path('jobs/', include(router.urls)),
    path('jobs/<int:job_pk>/tasks/<int:task_pk>/', include(urls))
]
