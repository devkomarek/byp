from rest_framework import viewsets

from . import serializers
from .models import Skill


class SkillViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.SkillSerializer
    queryset = Skill.objects.all()
