# Generated by Django 2.2.1 on 2019-06-29 10:44

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('goals', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Skill',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('default_exp', models.BigIntegerField()),
                ('skill_tree', models.ManyToManyField(to='goals.Goal')),
            ],
        ),
    ]
