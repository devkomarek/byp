from django.db import models


# Create your models here.

class Skill(models.Model):
    name = models.CharField(max_length=100)
    skill_tree = models.ManyToManyField('goals.Goal')
    default_exp = models.BigIntegerField()
