from rest_framework import routers

from .api import SkillViewSet

router = routers.DefaultRouter()
router.register('skills', SkillViewSet, 'skills')

urlpatterns = router.urls
